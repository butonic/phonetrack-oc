msgid ""
msgstr ""
"Project-Id-Version: phonetrack\n"
"Report-Msgid-Bugs-To: translations@owncloud.org\n"
"POT-Creation-Date: 2018-03-29 11:07+0200\n"
"PO-Revision-Date: 2018-03-29 05:24-0400\n"
"Last-Translator: eneiluj <eneiluj@posteo.net>\n"
"Language-Team: Polish\n"
"Language: pl_PL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=((n == 1) ? 0 : ((n%10 >= 2 && n%10 <=4 && (n%100 < 12 || n%100 > 14)) ? 1 : ((n%10 == 0 || n%10 == 1 || (n%10 >= 5 && n%10 <=9)) || (n%100 >= 12 && n%100 <= 14)) ? 2 : 3));\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: phonetrack\n"
"X-Crowdin-Language: pl\n"
"X-Crowdin-File: /master/l10n/templates/phonetrack.pot\n"

#: app.php:41
msgid "PhoneTrack"
msgstr "PhoneTrack"

#: logcontroller.php:200 logcontroller.php:223
msgid "Geofencing alert"
msgstr ""

#: logcontroller.php:203
#, php-format
msgid "In session \"%s\", device \"%s\" entered geofencing zone \"%s\"."
msgstr ""

#: logcontroller.php:226
#, php-format
msgid "In session \"%s\", device \"%s\" exited geofencing zone \"%s\"."
msgstr ""

#: leaflet.js:5
msgid "left"
msgstr "w lewo"

#: leaflet.js:5
msgid "right"
msgstr "w prawo"

#: phonetrack.js:544 maincontent.php:69
msgid "Show lines"
msgstr "Pokaż linie"

#: phonetrack.js:552
msgid "Hide lines"
msgstr "Ukryj linie"

#: phonetrack.js:573
msgid "Activate automatic zoom"
msgstr "Włacz automatyczne powiększanie"

#: phonetrack.js:581
msgid "Disable automatic zoom"
msgstr "Wyłącz automatyczne powiększanie"

#: phonetrack.js:602
msgid "Show last point tooltip"
msgstr "Pokaż ostatni punkt podpowiedzi"

#: phonetrack.js:610
msgid "Hide last point tooltip"
msgstr "Ukryj ostatni punkt podpowiedzi"

#: phonetrack.js:631
msgid "Zoom on all devices"
msgstr "Przybliż na wszystkich urządzeniach"

#: phonetrack.js:644
msgid "Click on the map to move the point, press ESC to cancel"
msgstr "Kliknij na mapie, aby przesunąć punkt, naciśnij klawisz ESC, aby anulować"

#: phonetrack.js:847
msgid "Server name or server url should not be empty"
msgstr "Nazwa serwera lub adres url serwera nie powinnny być puste"

#: phonetrack.js:850 phonetrack.js:859
msgid "Impossible to add tile server"
msgstr "Nie udało się dodać serwera fragmentów mapy"

#: phonetrack.js:856
msgid "A server with this name already exists"
msgstr "Serwer o tej nazwie już istnieje"

#: phonetrack.js:892 phonetrack.js:3210 maincontent.php:246 maincontent.php:290
#: maincontent.php:335 maincontent.php:384
msgid "Delete"
msgstr "Usuń"

#: phonetrack.js:925
msgid "Tile server \"{ts}\" has been added"
msgstr "Serwer fragmentów map \"{ts}\" został dodany"

#: phonetrack.js:928
msgid "Failed to add tile server \"{ts}\""
msgstr "Nie udało się dodać serwera fragmentów map \"{ts}\""

#: phonetrack.js:932
msgid "Failed to contact server to add tile server"
msgstr "Nie udało się połączyć z serwerem, aby dodać serwer fragmentów map"

#: phonetrack.js:966
msgid "Tile server \"{ts}\" has been deleted"
msgstr "Serwer fragmentów map \"{ts}\" został usunięty"

#: phonetrack.js:969
msgid "Failed to delete tile server \"{ts}\""
msgstr "Nie udało się usunąć serwera fragmentów map \"{ts}\""

#: phonetrack.js:973
msgid "Failed to contact server to delete tile server"
msgstr "Nie udało się połączyć z serwerem, aby usunąć serwer fragmentów map"

#: phonetrack.js:1104
msgid "Failed to contact server to restore options values"
msgstr "Nie udało się połączyć z serwerem, aby przywrócić wartości ustawień"

#: phonetrack.js:1107 phonetrack.js:1189 phonetrack.js:4703
msgid "Reload this page"
msgstr "Odśwież stronę"

#: phonetrack.js:1186
msgid "Failed to contact server to save options values"
msgstr "Nie udało się połączyć z serwerem, aby zapisać wartości ustawień"

#: phonetrack.js:1209
msgid "Session name should not be empty"
msgstr "Nazwa sesji nie powinna być pusta"

#: phonetrack.js:1226
msgid "Session name already used"
msgstr "Nazwa sesji jest już używana"

#: phonetrack.js:1230
msgid "Failed to contact server to create session"
msgstr "Nie udało się połączyć z serwerem, aby utworzyć sesję"

#: phonetrack.js:1325
msgid "Watch this session"
msgstr "Obejrzyj tę sesję"

#: phonetrack.js:1331
msgid "shared by {u}"
msgstr "udostępnione przez {u}"

#: phonetrack.js:1336 phonetrack.js:2477
msgid "More actions"
msgstr "Więcej akcji"

#: phonetrack.js:1340
msgid "Zoom on this session"
msgstr "Zbliżenie na tą sesję"

#: phonetrack.js:1343
msgid "URL to share session"
msgstr "Adres URL do udostępniania sesji"

#: phonetrack.js:1347
msgid "URLs for logging apps"
msgstr "Adresy URL dla rejestrowania aplikacji"

#: phonetrack.js:1351
msgid "Reserve device names"
msgstr "Zarezerwuj nazwy urządzeń"

#: phonetrack.js:1360
msgid "Delete session"
msgstr "Usuń sesję"

#: phonetrack.js:1361 phonetrack.js:1362
msgid "Rename session"
msgstr "Zmień nazwe sesji"

#: phonetrack.js:1364 phonetrack.js:1365
msgid "Export to gpx"
msgstr "Eksportuj do gpx"

#: phonetrack.js:1370
msgid "Files are created in '{exdir}'"
msgstr "Pliki są tworzone w '{exdir}'"

#: phonetrack.js:1371
msgid "Automatic export"
msgstr "Automatyczny eksport"

#: phonetrack.js:1373
msgid "never"
msgstr "nigdy"

#: phonetrack.js:1374
msgid "daily"
msgstr "dziennie"

#: phonetrack.js:1375
msgid "weekly"
msgstr "tygodniowo"

#: phonetrack.js:1376
msgid "monthly"
msgstr "miesięcznie"

#: phonetrack.js:1381
msgid "Automatic purge is triggered daily and will delete points older than selected duration"
msgstr ""

#: phonetrack.js:1382
msgid "Automatic purge"
msgstr ""

#: phonetrack.js:1384
msgid "don't purge"
msgstr ""

#: phonetrack.js:1385
msgid "a day"
msgstr "dzień"

#: phonetrack.js:1386
msgid "a week"
msgstr ""

#: phonetrack.js:1387
msgid "a month"
msgstr ""

#: phonetrack.js:1396
msgid "Name reservation is optional."
msgstr "Rezerwacja nazwy jest opcjonalna."

#: phonetrack.js:1397
msgid "Name can be set directly in logging URL if it is not reserved."
msgstr "Nazwę można ustawić bezpośrednio w adresie URL rejestrowania Jeśli nie jest zarezerwowana."

#: phonetrack.js:1398
msgid "To log with a reserved name, use its token in logging URL."
msgstr "Aby zalogować się przy użyciu zarezerwowanej nazwy, należy użyć jego token w adresie URL logowania."

#: phonetrack.js:1399
msgid "If a name is reserved, the only way to log with this name is with its token."
msgstr "Jeśli nazwa jest zarezerwowana, jedynym sposobem, aby zalogować się za pomocą tej nazwy jest z jego tokenem."

#: phonetrack.js:1402
msgid "Reserve this device name"
msgstr "Zarezerwuj tę nazwę urządzenia"

#: phonetrack.js:1404
msgid "Type reserved name and press 'Enter'"
msgstr "Wpisz zarezerwowaną nazwę użytkownika i naciśnij 'Enter'"

#: phonetrack.js:1418
msgid "Share with user"
msgstr "Udostępnij z użytkownikiem"

#: phonetrack.js:1420
msgid "Type user name and press 'Enter'"
msgstr "Wpisz nazwę użytkownika i naciśnij 'Enter'"

#: phonetrack.js:1425 phonetrack.js:3752
msgid "Shared with {u}"
msgstr "Udostępnione z {u}"

#: phonetrack.js:1431
msgid "A private session is not visible on public browser logging page"
msgstr "Prywatna sesja nie jest widoczna na publicznej stronie logowania"

#: phonetrack.js:1433 phonetrack.js:4712
msgid "Make session public"
msgstr "Przekształć sesje na publiczną"

#: phonetrack.js:1436 phonetrack.js:4707
msgid "Make session private"
msgstr "Przekształć sesje na prywatną"

#: phonetrack.js:1441
msgid "Public watch URL"
msgstr "Publiczny widok URL"

#: phonetrack.js:1443
msgid "API URL (JSON last positions)"
msgstr "API URL (ostatnie pozycje JSON)"

#: phonetrack.js:1449
msgid "Current active filters will be applied on shared view"
msgstr "Bieżące aktywne filtry zostanie zastosowany w widoku udostępnionym"

#: phonetrack.js:1451
msgid "Add public filtered share"
msgstr "Dodaj udziały publicznie filtrowane"

#: phonetrack.js:1461
msgid "List of server URLs to configure logging apps."
msgstr "Lista adresów URL serwera do skonfigurowanie rejestrowania aplikacji."

#: phonetrack.js:1462 maincontent.php:178
msgid "Replace 'yourname' with the desired device name or with the name reservation token"
msgstr "Zastąp 'yourname' z nazwą wybranego urządzenia lub z tokenem rezerwacji nazwy"

#: phonetrack.js:1464
msgid "Public browser logging URL"
msgstr "Adres URL rejestrowania publicznego przeglądarki"

#: phonetrack.js:1466
msgid "OsmAnd URL"
msgstr "OsmAnd URL"

#: phonetrack.js:1470
msgid "GpsLogger GET and POST URL"
msgstr "GpsLogger GET i POST URL"

#: phonetrack.js:1474
msgid "Owntracks (HTTP mode) URL"
msgstr "Adres URL Owntracks (tryb HTTP)"

#: phonetrack.js:1478
msgid "Ulogger URL"
msgstr "Ulogger URL"

#: phonetrack.js:1482
msgid "Traccar URL"
msgstr "Traccar URL"

#: phonetrack.js:1486
msgid "OpenGTS URL"
msgstr "OpenGTS URL"

#: phonetrack.js:1489
msgid "HTTP GET URL"
msgstr "HTTP GET URL"

#: phonetrack.js:1536
msgid "The session you want to delete does not exist"
msgstr "Sesje którą chcesz usunąć nie istnieje"

#: phonetrack.js:1539
msgid "Failed to delete session"
msgstr "Nie można usunąć sesji"

#: phonetrack.js:1543
msgid "Failed to contact server to delete session"
msgstr "Nie udało się połączyć do serwera, aby usunąć sesje"

#: phonetrack.js:1563
msgid "Device '{d}' of session '{s}' has been deleted"
msgstr "Urządzenia '{d}', sesji '{s}' został usunięte"

#: phonetrack.js:1566
msgid "Failed to delete device '{d}' of session '{s}'"
msgstr "Nie można usunąć urządzenia '{d}' sesji '{s}'"

#: phonetrack.js:1570
msgid "Failed to contact server to delete device"
msgstr "Nie udało się połączyć do serwera, aby usunąć urządzenie"

#: phonetrack.js:1622
msgid "Impossible to rename session"
msgstr "Niemożliwe, aby zmienić nazwę sesji"

#: phonetrack.js:1626
msgid "Failed to contact server to rename session"
msgstr "Nie udało się połączyć do serwera, aby zmienić nazwę sesji"

#: phonetrack.js:1674
msgid "Impossible to rename device"
msgstr "Niemożliwe, aby zmienić nazwę urządzenia"

#: phonetrack.js:1678
msgid "Failed to contact server to rename device"
msgstr "Nie udało się połączyć do serwera, aby zmienić nazwę urządzenia"

#: phonetrack.js:1735
msgid "Device already exists in target session"
msgstr "Urządzenie już istnieje w sesji docelowej"

#: phonetrack.js:1738
msgid "Impossible to move device to another session"
msgstr "Niemożliwe przenieść urządzenie do innej sesji"

#: phonetrack.js:1742
msgid "Failed to contact server to move device"
msgstr "Nie udało się połączyć do serwera, aby przesunąć urządzenie"

#: phonetrack.js:1809
msgid "Failed to contact server to get sessions"
msgstr "Nie udało się połączyć do serwera, aby uzyskać sesje"

#: phonetrack.js:2182 phonetrack.js:2216
msgid "Stats of all points"
msgstr "Statystyki wszystkich punktów"

#: phonetrack.js:2213
msgid "Stats of filtered points"
msgstr "Statystyki filtrowanych punktów"

#: phonetrack.js:2398
msgid "Device's color successfully changed"
msgstr "Kolor urządzenia pomyślnie zmieniony"

#: phonetrack.js:2401
msgid "Failed to save device's color"
msgstr "Nie można zapisać koloru urządzenia"

#: phonetrack.js:2405
msgid "Failed to contact server to change device's color"
msgstr "Nie udało się połączyć do serwera, aby zmienić kolor urządzenia"

#: phonetrack.js:2480
msgid "Delete this device"
msgstr "Usuń to urządzenie"

#: phonetrack.js:2482
msgid "Rename this device"
msgstr "Zmień nazwę tego urządzenia"

#: phonetrack.js:2485
msgid "Move to another session"
msgstr "Przenieść do innej sesji"

#: phonetrack.js:2488 maincontent.php:48
msgid "Ok"
msgstr "Ok"

#: phonetrack.js:2496
msgid "Device geofencing zones"
msgstr ""

#: phonetrack.js:2500
msgid "Use current map view as geofencing zone"
msgstr ""

#: phonetrack.js:2501
msgid "Add zone"
msgstr ""

#: phonetrack.js:2512
msgid "Toggle detail/edition points"
msgstr "Pokaż detale/edycje punktów"

#: phonetrack.js:2520
msgid "Toggle lines"
msgstr "Włącz linie"

#: phonetrack.js:2531 phonetrack.js:2537
msgid "Center map on device"
msgstr "Wyśrodkowanie mapy na urządzeniu"

#: phonetrack.js:2543
msgid "Follow this device (autozoom)"
msgstr "Obserwuj to urządzenie (autozoom)"

#: phonetrack.js:2822
msgid "The point you want to edit does not exist or you're not allowed to edit it"
msgstr "Punkt, który chcesz usunąć nie edytować lub nie możesz go edytować"

#: phonetrack.js:2826
msgid "Failed to contact server to edit point"
msgstr "Nie udało się połączyć do serwera, aby edytować punkt"

#: phonetrack.js:2942
msgid "The point you want to delete does not exist or you're not allowed to delete it"
msgstr "Punkt, który chcesz usunąć nie istnieje lub nie możesz go usunąć"

#: phonetrack.js:2946
msgid "Failed to contact server to delete point"
msgstr "Nie udało się połączyć do serwera, aby usunąć punkt"

#: phonetrack.js:3047
msgid "Impossible to add this point"
msgstr "Niemożliwe, aby dodać ten punkt"

#: phonetrack.js:3051
msgid "Failed to contact server to add point"
msgstr "Nie udało się połączyć do serwera, aby dodać punkt"

#: phonetrack.js:3166
msgid "Date"
msgstr "Data"

#: phonetrack.js:3169
msgid "Time"
msgstr "Czas"

#: phonetrack.js:3174 phonetrack.js:3226
msgid "Altitude"
msgstr "Wysokość npm"

#: phonetrack.js:3177 phonetrack.js:3230
msgid "Precision"
msgstr "Precyzja"

#: phonetrack.js:3180 phonetrack.js:3236
msgid "Speed"
msgstr ""

#: phonetrack.js:3188 phonetrack.js:3240
msgid "Bearing"
msgstr ""

#: phonetrack.js:3191 phonetrack.js:3244
msgid "Satellites"
msgstr "Satelity"

#: phonetrack.js:3194 phonetrack.js:3248
msgid "Battery"
msgstr "Bateria"

#: phonetrack.js:3197 phonetrack.js:3252
msgid "User-agent"
msgstr "User-agent"

#: phonetrack.js:3200
msgid "lat : lng"
msgstr "lat : lng"

#: phonetrack.js:3204
msgid "DMS coords"
msgstr "DMS coords"

#: phonetrack.js:3209
msgid "Save"
msgstr "Zapisz"

#: phonetrack.js:3211
msgid "Move"
msgstr "Przenieś"

#: phonetrack.js:3212
msgid "Cancel"
msgstr "Anuluj"

#: phonetrack.js:3430
msgid "File extension must be '.gpx' to be imported"
msgstr "Rozszerzenie pliku musi być '.gpx' aby zostało zaimportowane"

#: phonetrack.js:3448
msgid "Failed to create imported session"
msgstr "Nie udało się utworzyć zaimportowanej sesji"

#: phonetrack.js:3452 phonetrack.js:3458
msgid "Failed to import session"
msgstr "Nie udało się zaimportować sesji"

#: phonetrack.js:3453
msgid "File is not readable"
msgstr "Nie można odczytać pliku"

#: phonetrack.js:3459
msgid "File does not exist"
msgstr "Plik nie istnieje"

#: phonetrack.js:3467
msgid "Failed to contact server to import session"
msgstr "Nie udało się połączyć do serwera, aby importować sesje"

#: phonetrack.js:3487
msgid "Session successfully exported in"
msgstr "Sesja pomyślnie wyeksportowana"

#: phonetrack.js:3491
msgid "Failed to export session"
msgstr "Nie udało się eksportować sesji"

#: phonetrack.js:3496
msgid "Failed to contact server to export session"
msgstr "Nie udało się połączyć do serwera, aby eksportować sesje"

#: phonetrack.js:3527
msgid "Failed to contact server to log position"
msgstr "Nie udało się połączyć do serwera, aby rejestrować pozycje"

#: phonetrack.js:3675
msgid "'{n}' is already reserved"
msgstr "'{n}' jest już zarezerwowany"

#: phonetrack.js:3678
msgid "Failed to reserve '{n}'"
msgstr "Nie udało się zarezerwować '{n}'"

#: phonetrack.js:3681
msgid "Failed to contact server to reserve device name"
msgstr "Nie udało się połączyć do serwera, aby zarezerwować nazwę urządzenia"

#: phonetrack.js:3712 phonetrack.js:3716
msgid "Failed to delete reserved name"
msgstr "Nie udało się usunąć zarezerwowanej nazwy"

#: phonetrack.js:3713
msgid "This device does not exist"
msgstr "To urządzenie nie istnieje"

#: phonetrack.js:3717
msgid "This device name is not reserved, please reload this page"
msgstr "Ta nazwa urządzenia nie jest zarezerwowana, Załaduj ponownie Tę stronę"

#: phonetrack.js:3720
msgid "Failed to contact server to delete reserved name"
msgstr "Nie udało się połączyć do serwera, aby usunąć zarezerwowana nazwę"

#: phonetrack.js:3740
msgid "User does not exist"
msgstr "Użytkownik nie istnieje"

#: phonetrack.js:3743
msgid "Failed to add user share"
msgstr "Nie udało się dodać udziału użytkownika"

#: phonetrack.js:3746
msgid "Failed to contact server to add user share"
msgstr "Nie udało się połączyć do serwera, aby dodać udział użytkownika"

#: phonetrack.js:3777
msgid "Failed to delete user share"
msgstr "Nie udało się usunąć udziału użytkownika"

#: phonetrack.js:3780
msgid "Failed to contact server to delete user share"
msgstr "Nie udało się połączyć do serwera, aby usunąć udział użytkownika"

#: phonetrack.js:3798 phonetrack.js:3822
msgid "Public share has been successfully modified"
msgstr ""

#: phonetrack.js:3801 phonetrack.js:3825
msgid "Failed to modify public share"
msgstr ""

#: phonetrack.js:3804 phonetrack.js:3828
msgid "Failed to contact server to modify public share"
msgstr ""

#: phonetrack.js:3846
msgid "Device name restriction has been successfully set"
msgstr ""

#: phonetrack.js:3849
msgid "Failed to set public share device name restriction"
msgstr ""

#: phonetrack.js:3852
msgid "Failed to contact server to set public share device name restriction"
msgstr ""

#: phonetrack.js:3880
msgid "Warning : User email and server admin email must be set to receive geofencing alerts."
msgstr ""

#: phonetrack.js:3884
msgid "Failed to add geofencing zone"
msgstr ""

#: phonetrack.js:3887
msgid "Failed to contact server to add geofencing zone"
msgstr ""

#: phonetrack.js:3921
msgid "Failed to delete geofencing zone"
msgstr ""

#: phonetrack.js:3924
msgid "Failed to contact server to delete geofencing zone"
msgstr ""

#: phonetrack.js:3943
msgid "Failed to add public share"
msgstr "Nie udało się dodać udziału publicznego"

#: phonetrack.js:3946
msgid "Failed to contact server to add public share"
msgstr "Nie udało się połączyć do serwera, aby dodać udział publiczny"

#: phonetrack.js:3965
msgid "Show this device only"
msgstr ""

#: phonetrack.js:3967
msgid "Show last positions only"
msgstr ""

#: phonetrack.js:3969
msgid "Simplify positions to nearest geofencing zone center"
msgstr ""

#: phonetrack.js:3988
msgid "No filters"
msgstr "Brak filtrów"

#: phonetrack.js:4012
msgid "Failed to delete public share"
msgstr "Nie udało się usunąć udziału publicznego"

#: phonetrack.js:4015
msgid "Failed to contact server to delete public share"
msgstr "Nie udało się połączyć do serwera, aby usunąć udział publiczny"

#: phonetrack.js:4033
msgid "Failed to contact server to get user list"
msgstr "Nie udało się połączyć do serwera, aby zdobyć liste użytkowników"

#: phonetrack.js:4046
msgid "device name"
msgstr "nazwa urządzenia"

#: phonetrack.js:4047
msgid "distance (km)"
msgstr "odległość (km)"

#: phonetrack.js:4048
msgid "duration"
msgstr "czas trwania"

#: phonetrack.js:4049
msgid "#points"
msgstr "#punkty"

#: phonetrack.js:4091
msgid "years"
msgstr "lata"

#: phonetrack.js:4094
msgid "days"
msgstr "dni"

#: phonetrack.js:4113
msgid "In OsmAnd, go to 'Plugins' in the main menu, then activate 'Trip recording' plugin and go to its settings."
msgstr "W głównym menu OsmAnd przejdź do 'Wtyczki', a następnie aktywuj wtyczkę 'Nagrywanie trasy' i przejdź do jej ustawień."

#: phonetrack.js:4114
msgid "Copy the URL below into the 'Online tracking web address' field."
msgstr "Skopiuj poniższy adres URL do pola 'Adres sieciowy śledzenia online'."

#: phonetrack.js:4118
msgid "In GpsLogger, go to 'Logging details' in the sidebar menu, then activate 'Log to custom URL'."
msgstr "W GpsLogger przejdź do 'Rejestrowanie szczegółów' w menu bocznym, a następnie aktywuj 'Rejestruj w niestandardowym URL'."

#: phonetrack.js:4119
msgid "Copy the URL below into the 'URL' field."
msgstr "Skopiuj poniższy adres URL do pola 'URL'."

#: phonetrack.js:4127
msgid "In Ulogger, go to settings menu and copy the URL below into the 'Server URL' field."
msgstr "Przejdź do menu Ustawienia w Ulogger i skopiuj poniższy adres URL do pola 'Adres URL serwera'."

#: phonetrack.js:4128
msgid "Set 'User name' and 'Password' mandatory fields to any value as they will be ignored by PhoneTrack."
msgstr "Ustaw dowolną wartość pól obowiązkowych 'Nazwa użytkownika' i 'Hasło', będą one ignorowane przez PhoneTrack."

#: phonetrack.js:4129
msgid "Activate 'Live synchronization'."
msgstr "Aktywuj 'Synchronizacja w czasie rzeczywistym'."

#: phonetrack.js:4133
msgid "In Traccar client, copy the URL below into the 'server URL' field."
msgstr "W kliencie Traccar skopiuj poniższy adres URL do pola 'adres URL serwera'."

#: phonetrack.js:4137
msgid "You can log with any other client with a simple HTTP request."
msgstr "Możesz rejestrować z innego klienta przy użyciu prostego żądania HTTP."

#: phonetrack.js:4138
msgid "Make sure the logging system sets values for at least 'timestamp', 'lat' and 'lon' GET parameters."
msgstr "Upewnij się, że system logowania ustawia wartości co najmniej parametrów GET 'timestamp', 'lat' i 'lon'."

#: phonetrack.js:4141
msgid "Configure {loggingApp} for logging to session '{sessionName}'"
msgstr "Skonfiguruj {loggingApp} do logowania się do sesji '{sessionName}'"

#: phonetrack.js:4272
msgid "Are you sure you want to delete the session {session} ?"
msgstr "Czy na pewno chcesz usunąć tą sesje {session}?"

#: phonetrack.js:4275
msgid "Confirm session deletion"
msgstr "Potwierdź usunięcie sesji"

#: phonetrack.js:4333
msgid "Choose auto export target path"
msgstr "Wybierz obiekt docelowy ścieżki automatycznego eksportu"

#: phonetrack.js:4471
msgid "Select storage location for '{fname}'"
msgstr "Wybierz lokalizację przechowywania dla '{fname}'"

#: phonetrack.js:4638
msgid "Are you sure you want to delete the device {device} ?"
msgstr "Czy na pewno chcesz usunąć to urządzenie {device}?"

#: phonetrack.js:4641
msgid "Confirm device deletion"
msgstr "Potwierdź usunięcie urządzenia"

#: phonetrack.js:4699
msgid "Failed to toggle session public status, session does not exist"
msgstr "Nie udało się pokazać statusu publicznego, sesja nie istnieje"

#: phonetrack.js:4702
msgid "Failed to contact server to toggle session public status"
msgstr "Nie udało się połączyć do serwera, aby pokazać status publiczny"

#: phonetrack.js:4735
msgid "Failed to set session auto export value"
msgstr "Nie można ustawić wartości auto-eksportu sesji"

#: phonetrack.js:4736 phonetrack.js:4763
msgid "session does not exist"
msgstr "sesja nie istnieje"

#: phonetrack.js:4740
msgid "Failed to contact server to set session auto export value"
msgstr "Nie udało połączyć się z serwerem aby ustawić wartości auto-eksportu sesji"

#: phonetrack.js:4762
msgid "Failed to set session auto purge value"
msgstr ""

#: phonetrack.js:4767
msgid "Failed to contact server to set session auto purge value"
msgstr ""

#: phonetrack.js:4850
msgid "Import gpx session file"
msgstr "Importuj plik sesji gpx"

#: maincontent.php:4
msgid "Main tab"
msgstr "Karta główna"

#: maincontent.php:5
msgid "Filters"
msgstr "Filtry"

#: maincontent.php:14
msgid "Stats"
msgstr "Statystyki"

#: maincontent.php:15 maincontent.php:211
msgid "Settings and extra actions"
msgstr "Ustawienia i działania dodatkowe"

#: maincontent.php:16 maincontent.php:605
msgid "About PhoneTrack"
msgstr "O PhoneTrack"

#: maincontent.php:36
msgid "Import session"
msgstr "Importuj sesje"

#: maincontent.php:40
msgid "Create session"
msgstr "Utwórz sesje"

#: maincontent.php:44
msgid "Session name"
msgstr "Nazwa sesji"

#: maincontent.php:57
msgid "Options"
msgstr "Opcje"

#: maincontent.php:65
msgid "Auto zoom"
msgstr "Automatyczne przybliżenie"

#: maincontent.php:73
msgid "Show tooltips"
msgstr "Pokaż podpowiedzi"

#: maincontent.php:76
msgid "Refresh each (sec)"
msgstr "Odświeżanie każdego (sek)"

#: maincontent.php:79
msgid "Refresh"
msgstr "Odśwież"

#: maincontent.php:81 maincontent.php:86
msgid "Cutting lines only affects map view and stats table"
msgstr ""

#: maincontent.php:83
msgid "Minimum distance to cut between two points"
msgstr ""

#: maincontent.php:84
msgid "meters"
msgstr ""

#: maincontent.php:88
msgid "Minimum time to cut between two points"
msgstr ""

#: maincontent.php:89
msgid "seconds"
msgstr ""

#: maincontent.php:93
msgid "Show accuracy in tooltips"
msgstr "Pokaż dokładność w podpowiedziach"

#: maincontent.php:97
msgid "Show speed in tooltips"
msgstr ""

#: maincontent.php:101
msgid "Show bearing in tooltips"
msgstr ""

#: maincontent.php:105
msgid "Show satellites in tooltips"
msgstr "Pokaż satelity w podpowiedziach"

#: maincontent.php:109
msgid "Show battery level in tooltips"
msgstr "Pokaż poziom baterii w podpowiedziach"

#: maincontent.php:113
msgid "Show elevation in tooltips"
msgstr "Pokaż elewacje w podpowiedziach"

#: maincontent.php:117
msgid "Show user-agent in tooltips"
msgstr "Pokaż user-agent w podpowiedziach"

#: maincontent.php:122
msgid "Make points draggable in edition mode"
msgstr "Spraw aby można było przesunąć punkty w trybie edycji"

#: maincontent.php:126
msgid "Show accuracy circle on hover"
msgstr "Pokaż okrąg dokładność przy aktywowaniu"

#: maincontent.php:129
msgid "Line width"
msgstr "Szerekość linii"

#: maincontent.php:133
msgid "Point radius"
msgstr "Promień punktu"

#: maincontent.php:137
msgid "Points and lines opacity"
msgstr "Punkty i linie krycie"

#: maincontent.php:141
msgid "Theme"
msgstr "Motyw"

#: maincontent.php:143
msgid "bright"
msgstr "jasny"

#: maincontent.php:144
msgid "pastel"
msgstr "pastelowy"

#: maincontent.php:145
msgid "dark"
msgstr "ciemny"

#: maincontent.php:149
msgid "Auto export path"
msgstr "Ścieżka automatycznego eksportu"

#: maincontent.php:154
msgid "Export one file per device"
msgstr ""

#: maincontent.php:156
msgid "reload page to make changes effective"
msgstr "odśwież stronę aby zmiany stały się efektywne"

#: maincontent.php:163
msgid "Device name"
msgstr "Nazwa urządzenia"

#: maincontent.php:165
msgid "Log my position in this session"
msgstr "Rejestruj moją pozycje podczas tej sesji"

#: maincontent.php:170
msgid "Tracking sessions"
msgstr "Śledzenie sesji"

#: maincontent.php:216
msgid "Custom tile servers"
msgstr "Niestandardowy kafelek serwerowy"

#: maincontent.php:219 maincontent.php:259 maincontent.php:302
#: maincontent.php:347
msgid "Server name"
msgstr "Nazwa serwera"

#: maincontent.php:220 maincontent.php:260 maincontent.php:303
#: maincontent.php:348
msgid "For example : my custom server"
msgstr "Na przykład: mój niestandardowy serwer"

#: maincontent.php:221 maincontent.php:261 maincontent.php:304
#: maincontent.php:349
msgid "Server url"
msgstr "Adres Url serwera"

#: maincontent.php:222 maincontent.php:305
msgid "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png"
msgstr "Na przykład: http://tile.server.org/cycle/{z}/{x}/{y}.png"

#: maincontent.php:223 maincontent.php:263 maincontent.php:306
#: maincontent.php:351
msgid "Min zoom (1-20)"
msgstr "Powiększenie min (1-20)"

#: maincontent.php:225 maincontent.php:265 maincontent.php:308
#: maincontent.php:353
msgid "Max zoom (1-20)"
msgstr "Powiększenie max (1-20)"

#: maincontent.php:227 maincontent.php:271 maincontent.php:316
#: maincontent.php:365
msgid "Add"
msgstr "Dodaj"

#: maincontent.php:230
msgid "Your tile servers"
msgstr "Twoje kafelki serwerów"

#: maincontent.php:256
msgid "Custom overlay tile servers"
msgstr "Niestandardowe nakładki kafelek na serwery"

#: maincontent.php:262 maincontent.php:350
msgid "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png"
msgstr "Na przykład: http://overlay.server.org/cycle/{z}/{x}/{y}.png"

#: maincontent.php:267 maincontent.php:355
msgid "Transparent"
msgstr "Przezroczysty"

#: maincontent.php:269 maincontent.php:357
msgid "Opacity (0.0-1.0)"
msgstr "Krycie (0,0-1,0)"

#: maincontent.php:274
msgid "Your overlay tile servers"
msgstr "Twoje nakładki kafelków na serwerach"

#: maincontent.php:299
msgid "Custom WMS tile servers"
msgstr "Niestandardowe kafelki serwerowe WMS"

#: maincontent.php:310 maincontent.php:359
msgid "Format"
msgstr "Format"

#: maincontent.php:312 maincontent.php:361
msgid "WMS version"
msgstr "Wersja WMS"

#: maincontent.php:314 maincontent.php:363
msgid "Layers to display"
msgstr "Warstwy, do wyświetlenia"

#: maincontent.php:319
msgid "Your WMS tile servers"
msgstr "Twoje kafelki WMS serwerów"

#: maincontent.php:344
msgid "Custom WMS overlay servers"
msgstr "Niestandardowe kafelki WMS serwerów"

#: maincontent.php:368
msgid "Your WMS overlay tile servers"
msgstr "Twoje nakładki kafelków WMS na serwerach"

#: maincontent.php:397
msgid "Manually add a point"
msgstr "Ręcznie Dodaj punkt"

#: maincontent.php:399 maincontent.php:417
msgid "Session"
msgstr "Sesja"

#: maincontent.php:403 maincontent.php:421
msgid "Device"
msgstr "Urządzenie"

#: maincontent.php:405
msgid "Add a point"
msgstr "Dodaj punkt"

#: maincontent.php:406
msgid "Now, click on the map to add a point (if session is not activated, you won't see added point)"
msgstr "Teraz, kliknij na mapę, aby dodać punkt (Jeśli sesja nie jest aktywowana, nie zobaczysz dodanego punktu)"

#: maincontent.php:407
msgid "Cancel add point"
msgstr "Anuluj Dodanie punktu"

#: maincontent.php:412
msgid "Delete multiple points"
msgstr "Dodaj wiele punktów"

#: maincontent.php:415
msgid "Choose a session, a device and adjust the filters. All displayed points for selected device will be deleted. An empty device name selects them all."
msgstr "Wybierz sesję, urządzenie i ustaw filtry. Wszystkie wyświetlone punkty dla wybranego urządzenia zostaną usunięte. Pusta nazwa urządzenia zaznacza je wszystkie."

#: maincontent.php:423
msgid "Delete points"
msgstr "Usuń punkty"

#: maincontent.php:424
msgid "Delete only visible points"
msgstr "Usuń tylko widoczne punkty"

#: maincontent.php:431
msgid "Filter points"
msgstr "Punkty filtra"

#: maincontent.php:436
msgid "Apply filters"
msgstr "Zastosuj filtry"

#: maincontent.php:440
msgid "Begin date"
msgstr "Data rozpoczęcia"

#: maincontent.php:446 maincontent.php:465
msgid "today"
msgstr "dziś"

#: maincontent.php:452
msgid "Begin time"
msgstr "Czas rozpoczęcia"

#: maincontent.php:459
msgid "End date"
msgstr "Data zakończenia"

#: maincontent.php:471
msgid "End time"
msgstr "Czas zakończenia"

#: maincontent.php:478
msgid "Min-- and Max--"
msgstr "Min--i Max--"

#: maincontent.php:481
msgid "Min++ and Max++"
msgstr "Min ++ i Max ++"

#: maincontent.php:485
msgid "Last day:hour:min"
msgstr "Ostatni dzień: godzina:min"

#: maincontent.php:493
msgid "Minimum accuracy"
msgstr "Minimalna dokładność"

#: maincontent.php:501
msgid "Maximum accuracy"
msgstr "Maksymalna dokładność"

#: maincontent.php:509
msgid "Minimum elevation"
msgstr "Minimalna Elewacja"

#: maincontent.php:517
msgid "Maximum elevation"
msgstr "Maksymalna elewacja"

#: maincontent.php:525
msgid "Minimum battery level"
msgstr "Minimalny poziom baterii"

#: maincontent.php:533
msgid "Maximum battery level"
msgstr "Maksymalny poziom baterii"

#: maincontent.php:541
msgid "Minimum speed"
msgstr ""

#: maincontent.php:549
msgid "Maximum speed"
msgstr ""

#: maincontent.php:557
msgid "Minimum bearing"
msgstr ""

#: maincontent.php:565
msgid "Maximum bearing"
msgstr ""

#: maincontent.php:573
msgid "Minimum satellites"
msgstr "Minimum Satelitów"

#: maincontent.php:581
msgid "Maximum satellites"
msgstr "Maksimum Satelitów"

#: maincontent.php:593
msgid "Statistics"
msgstr "Statystyki"

#: maincontent.php:597
msgid "Show stats"
msgstr "Pokaż statystyki"

#: maincontent.php:607
msgid "Shortcuts"
msgstr "Skróty"

#: maincontent.php:609
msgid "Toggle sidebar"
msgstr "Rozwiń boczne menu"

#: maincontent.php:613
msgid "Documentation"
msgstr "Dokumentacja"

#: maincontent.php:621
msgid "Source management"
msgstr "Zarządzanie źródłem"

#: maincontent.php:635
msgid "Authors"
msgstr "Autorzy"

